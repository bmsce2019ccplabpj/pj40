#include <stdio.h>

int input()
{
    int n;
    printf("Enter the number\n");
    scanf("%d",&n);
    return n;
}

int compute(int n)
{
    int d,rev=0;
    while(n>0)
    {
        d=n%10;
        rev=rev*10+d;
        n=n/10;
    }
    return rev;
}

void output(int n,int rev)
{
    printf("Reverse of the number = %d\n",rev);
    if(n==rev)
        printf("%d is a palindrome number\n",n);
    else
        printf("%d is not a palindrome number\n",n);
}

int main()
{
    int n,rev;
    n=input();
    rev=compute(n);
    output(n,rev);
    return 0;
}